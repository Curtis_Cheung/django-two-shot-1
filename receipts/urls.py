from django.urls import path
from receipts.views import ReceiptView


urlpatterns = [
    path("", ReceiptView.as_view(), name="home")
]
