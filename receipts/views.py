from django.shortcuts import render
from django.views.generic.list import ListView

from receipts.models import Receipt
from django.views.generic.base import RedirectView


class ReceiptView(ListView):
    model = Receipt
    template_name = "receipts_list.html"


class ReceiptRedirectView(RedirectView):
    url = "/receipts/"
    pattern_name = "home"
